@extends('layoutss.main')

@section('content')

    <body>

        <section class="home"><br>
            <!-- <div class="text">Dashboard</div> -->
            <div class="container">
                <h1>TABEL DATA MENU</h1>
                <!-- Di sini Anda dapat menambahkan tabel data menu jika diperlukan -->
            </div><br>
            <div class="outer-container">
                <a href="/menus/create"><button class="coklat-btn1"><i class='bx bxs-message-square-add'></i></i> Add data
                        menu</button></a>
                <div class="inner-container">
                    <table>
                        <thead>
                            <tr>
                                <th>ID Menu</th>
                                <th>Nomor Menu</th>
                                <th>Nama Menu</th>
                                <th>Deskripsi</th>
                                <th>Harga</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($menu as $m)
                                <tr>
                                    <td>{{ $m->ID_menu }}</td>
                                    <td>{{ $m->No_menu }}</td>
                                    <td>{{ $m->Nama_menu }}</td>
                                    <td>{{ $m->Deskripsi }}</td>
                                    <td>{{ $m->Harga }}</td>
                                    <td><a href="/menus/update/{{ $m->ID_menu }}"><button class="coklat-btn"><i class='bx bxs-edit bx-tada' ></i></i> Update data</button></a></td>
                                    <td><a href="/menu/delete/{{ $m->ID_menu }}"><button class="merah-btn"><i
                                                    class='bx bxs-trash icon'></i> Delete</button></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </body>
@endsection
