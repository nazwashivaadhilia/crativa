<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INPUT DATA CONTACT</title>
</head>

<body>
    <h1>Tambah contact</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="/contact/storecontact" method="POST" enctype="multipart/form-data">
        @csrf

        <label for="ID_contact">ID contact:</label>
        <input type="text" name="ID_contact" id="kode_head" value="{{ old('ID_contact') }}"><br><br>

        <label for="detail">Detail:</label>
        <input type="text" name="detail" id="judul" value="{{ old('detail') }}"><br><br>

        <label for="des">Deskripsi:</label>
        <input type="text" name="des" id="judul" value="{{ old('des') }}"><br><br>

        <label for="no">Telfon:</label>
        <input type="text" name="no" id="judul" value="{{ old('no') }}"><br><br>

        <label for="email">Email:</label>
        <input type="email" name="email" id="judul" value="{{ old('email') }}"><br><br>

        <label for="loc">Location:</label>
        <input type="text" name="loc" id="judul" value="{{ old('loc') }}"><br><br>

        <button type="submit">Simpan</button>
    </form>
</body>

</html>
