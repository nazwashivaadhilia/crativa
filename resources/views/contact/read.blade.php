@extends('layoutss.main')

@section('content')

    <body>

        <section class="home"><br>
            <!-- <div class="text">Dashboard</div> -->
            <div class="container">
                <h1>DATA CONTACT CRATIVA</h1>
                <!-- Di sini Anda dapat menambahkan tabel data menu jika diperlukan -->
            </div><br><br>
            <div class="container-contact">
                <div class="contact-details">
                    @foreach ($contact as $c)
                        <div class="contact-item">
                            <i class='bx bxs-detail bx-tada' style='color:#7e3302' ></i>
                            <span><strong>{{ $c->detail }}</strong></span>
                        </div>
                        <div class="contact-item">
                            <i class='bx bx-list-ol bx-tada' style='color:#7e3302' ></i>
                            <span><strong>{{ $c->des }}</strong></span>
                        </div>
                        <div class="contact-item">
                            <i class='bx bxs-phone-call bx-tada' style='color:#7e3302' ></i>
                            <span><strong>{{ $c->no }}</strong></span>
                        </div>
                        <div class="contact-item">
                            <i class='bx bxs-envelope bx-tada' style='color:#7e3302' ></i>
                            <span><strong>{{ $c->email }}</strong></span>
                        </div>
                        <div class="contact-item">
                            <i class='bx bxs-location-plus bx-tada' style='color:#7e3302' ></i>
                            <span><strong>{{ $c->loc }}</strong></span>
                        </div>
                    @endforeach
                </div>
                <a href="/contact/update/{{ $c->ID_contact }}"><button class="contact-button"><i class='bx bxs-edit bx-tada' ></i> Update data</button></a>
            </div>

        </section>
    </body>
@endsection











