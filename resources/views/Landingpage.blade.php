<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Fonts Google - Rubik -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500&display=swap" rel="stylesheet">
    <!-- Icofont -->
    <link rel="stylesheet" href="css/icofont.min.css">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="css/styles.css">
    <!-- Title -->
    <title>Crativa Template</title>
</head>

<body>
    <!-- Navigation -->
    <div class="navbar">
        <div class="navbar-logo">
            <h1>CRATIVA</h1>
        </div>
        <div class="navbar-menu">
            <ul>
                <li><a href="{{ url('/sesi') }}">Login</a></li>
                <li><a href="{{ url('/sesi/register') }}">Register</a></li>
            </ul>
        </div>
    </div><br><br><br>
    <!-- Section > Home Banner -->
    <div class="d-block py-lg-5 py-3">
        <div class="section-have-cover container my-lg-5 my-4 px-lg-5">
            <div class="row align-items-center">
                <div class="col-md-6 mb-lg-0 mb-4">
                    @foreach ($heads as $h)
                        <div class="block-img rounded shadow"
                            style="background-image: url('{{ asset('images/' . $h->gambar) }}');"></div>
                    @endforeach
                </div>
                <div class="col-md-6 ">
                    <div class="d-block ps-lg-5">
                        @foreach ($heads as $h)
                            <h1 class="mb-3 text-xlg">{{ $h->judul }}</h1>
                        @endforeach
                        @foreach ($heads as $h)
                            <p class="mb-4 font-regular">{{ $h->deskripsi }}</p>
                        @endforeach
                        <a href="#" class="btn btn-lg btn-primary px-4">Hire Me <i
                                class="icofont-arrow-right ms-3"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section > Text -->
    <div id="featuresSection" class="d-block bg-secondary py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5 text-center">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h2 class="d-block mb-4">
                        I've created tons of beautiful flower/bucket for your events
                    </h2>
                    <a href="#" class="btn btn-outline-primary px-5">Learn More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="d-block section-xlg bg-images" style="background-image: url('img/banner-2.jpeg');"></div>
    <!-- Section > Content -->
    <div class="d-block bg-light py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5">
            <div class="row align-items-center">
                <div class="col-md-6 order-md-2 mb-lg-0 mb-4">
                    @foreach ($infos as $info)
                        <div class="block-img rounded"
                            style="background-image: url('{{ asset('images/' . $info->gambar) }}');"></div>
                    @endforeach
                </div>
                <div class="col-md-6 order-md-1 mb-lg-0 mb-4">
                    <div class="d-block pe-lg-5">
                        <div class="d-block mb-4">About Me</div>
                        @foreach ($infos as $info)
                            <h2 class="mb-3">{{ $info->judul }}</h2>
                        @endforeach
                        @foreach ($infos as $info)
                            <p class="mb-3">{{ $info->deskripsi }}</p>
                        @endforeach
                        <ul class="list-group list-group-flush">
                            @foreach ($infos as $info)
                                <li class="list-group-item bg-light border-primary px-0">{{ $info->keterangan }}</li>
                            @endforeach
                            <li class="list-group-item bg-light border-primary px-0"></li>
                            {{-- <li class="list-group-item bg-light border-primary px-0">I have 5 teams to work with you
                            </li> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section > Feature -->
    {{-- <div id="featuresSection" class="d-block py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5">
            <h2 class="mb-lg-5 mb-4">Item Menu</h2>
            <div class="row">
                @foreach ($menu as $m)
                    <div class="col-md-4 mb-lg-0 mb-3">
                        <div class="d-flex flex-column pb-3 mb-3 border-bottom border-primary h-100">
                            <div class="text-lg font-thin text-primary d-block mb-3">{{ $m->No_menu }}</div>
                            <div class="flex-1">
                                <h4 class="mb-3">{{ $m->Nama_menu }}</h4>
                                <p class="small">{{ $m->Deskripsi }}</p>
                                <p>Start from ${{ $m->Harga }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div> --}}
    <br><br>
    {{-- MENUUUUU --}}

    <div class="container1">
        @foreach ($menu as $m)
            <div class="card">
                <div class="content">
                    <div class="position-number">{{ $m->No_menu }}</div>
                    <br>
                    <h1 class="namamenu">{{ $m->Nama_menu }}</h1>
                    <div class="garis"></div>
                    <p class="des">{{ $m->Deskripsi }}</p>
                    <p class="price">Start from ${{ $m->Harga }}</p>
                </div>
            </div>
        @endforeach
    </div>




    <!-- Section -->
    <div class="d-block bg-dark text-white py-lg-5 py-3">
        <div class="container my-lg-5 my-4 px-lg-5 text-center">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @foreach ($kuote as $k)
                        <h2 class="d-block font-thin mb-5">
                            {{ $k->kuote }}
                        </h2>
                    @endforeach
                    <div class="d-flex align-items-center justify-content-center">
                        <a href="#" class="btn btn-outline-light">Hire Me</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Section > Content -->
    <div class="d-block bg-images py-lg-5 py-3 text-white" style="background-image: url('img/banner-4.jpeg');">
        <div class="section-have-cover container my-lg-5 my-4 px-lg-5 text-center">
            <h1 class="mt-0 mb-3"><i class="icofont-phone me-2"></i></h1>
            @foreach ($contact as $c)
                <h2 class="mt-0 mb-2">{{ $c->detail }}</h2>
            @endforeach
            @foreach ($contact as $c)
                <p class="small mt-0 mb-4">{{ $c->des }}</p>
            @endforeach
            <a href="#" class="btn btn-primary px-4">Hire Me</a>
        </div>
        <div class="bg-images-cover"></div>
    </div>
    <div class="d-block py-lg-5 py-3">
        <div class="container px-lg-5">
            <div class="d-flex d-flex-mobile align-items-start justify-content-center">
                <div class="mx-3 text-center mb-lg-0 mb-3">
                    <small class="font-weight-bold"><i class="icofont-phone me-2"></i>Phone</small>
                    @foreach ($contact as $c)
                        <div class="d-block font-weight-bold">{{ $c->no }}</div>
                    @endforeach
                </div>
                <div class="mx-3 text-center mb-lg-0 mb-3">
                    <small class="font-weight-bold"><i class="icofont-envelope me-2"></i>Email</small>
                    @foreach ($contact as $c)
                        <div class="d-block font-weight-bold">{{ $c->email }}</div>
                    @endforeach
                </div>
                <div class="mx-3 text-center mb-lg-0 mb-3">
                    <small class="font-weight-bold"><i class="icofont-location-pin me-2"></i>Location</small>
                    @foreach ($contact as $c)
                        <div class="d-block font-weight-bold">{{ $c->loc }}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Navigation -->
    <div class="nav-top px-lg-4 px-2 pb-5 pt-5">
        <div class="nav-inner h-100">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <div class="col-4 h-100">
                        <div class="d-flex align-items-center h-100">
                            <div class="nav-top-menus">
                                <a href="#featuresSection" class="menus-link me-3">About</a>
                                <a href="#pricingSection" class="menus-link me-3">Skill</a>
                                <a href="#contactSection" class="menus-link">Contact</a>
                                <a href="#contactSection" class="menus-link">logout</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-4 h-100">
                        <div class="d-flex justify-content-center align-items-center h-100">
                            <a href="#" class="nav-logo nav-logo-lg">
                                Crativa
                            </a>
                        </div>
                    </div>
                    <div class="col-4 h-100">
                        <div class="d-flex align-items-center justify-content-end h-100">
                            <div class="nav-top-menus">
                                <a href="#" class="menus-link me-3"><i class="icofont-instagram"></i></a>
                                <a href="#" class="menus-link"><i class="icofont-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
