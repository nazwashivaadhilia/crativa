<nav class="sidebar close">
    <header>
        <div class="image-text">
            <span class="image">
                <img src="{{ asset('img/loggo.png') }}" alt="logo">
            </span>

            <div class="text header-text">
                <span class="name">Crativa</span>
                <span class="profession">Web developer</span>
            </div>
        </div>

        <i class='bx bx-chevron-right toggle'></i>
    </header>

    <div class="menu-bar">
        <div class="menu">
            <!-- <li class="nav-link">
                <i class='bx bx-search-alt icon'></i>
                <input type="search" placeholder="Search...">
            </li> -->
            <ul class="menu-links">
                <li class="nav-links">
                    <a href="/dashboard">
                        <i class='bx bx-menu icon' ></i>
                        <span class="text nav-text"> Dashboard</span>
                    </a>
                </li>
                <li class="nav-links">
                    <a href="/menus/showmenu">
                        <i class='bx bxs-food-menu icon' ></i>
                        <span class="text nav-text"> Data menu</span>
                    </a>
                </li>
                <li class="nav-links">
                    <a href="/head/read">
                        <i class='bx bxs-file icon' ></i>
                        <span class="text nav-text"> Data head</span>
                    </a>
                </li>
                <li class="nav-links">
                    <a href="/contact/read">
                        <i class='bx bxs-contact icon' ></i>
                        <span class="text nav-text"> Data contact</span>
                    </a>
                </li>
                <li class="nav-links">
                    <a href="/kuote/read">
                        <i class='bx bxs-quote-right icon' ></i>
                        <span class="text nav-text"> Data Quote</span>
                    </a>
                </li>
                <li class="nav-links">
                    <a href="/info/index">
                        <i class='bx bxs-info-circle icon'></i>
                        <span class="text nav-text"> Data info</span>
                    </a>
                </li>
                <li class="nav-links">
                    <a href="/sesi/logout">
                        <i class='bx bx-log-out icon'></i>
                        <span class="text nav-text"> Logout</span>
                    </a>
                </li>
            </ul>
        </div>

    </div>
</nav>
