<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard admin</title>
    <!-- css -->
    <link rel="stylesheet" href="style.css">
    <!-- --icon-- -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');

        * {
            font-family: 'Poppins', sans-serif;
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        :root {
            /* ------warna------ */
            --body-color: #f8f2ee;
            --sidebar-color: #FFF;
            --primary-color: #7E3302;
            --body-color-light: #F6F5FF;
            --toggle-color: #DDD;
            --text-color: #707070;

            /* ------transisi------ */
            --tran-02: all 0.2s ease;
            --tran-03: all 0.3s ease;
            --tran-04: all 0.4s ease;
            --tran-05: all 0.5s ease;
        }

        body {
            height: 100xh;
            background: var(--body-color);
        }


        /* css sidebar */
        .sidebar .text {
            font-size: 16px;
            font-weight: 500;
            color: var(--text-color);
            transition: var(--tran-04);
            white-space: nowrap;
            opacity: 1;
        }

        .sidebar .image {
            min-width: 60px;
            display: flex;
            align-items: center;
        }

        .sidebar {
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 250px;
            padding: 10px 14px;
            background: var(--sidebar-color);
            transition: var(--tran-05);
            z-index: 100;
        }

        /* .sidebar.close .text {
            opacity: 0;
        }

        .sidebar.close {
            width: 72px;
        }  */

        .sidebar li {
            height: 50px;
            margin-top: 10px;
            list-style: none;
            display: flex;
            align-items: center;
        }

        .sidebar li .icon {
            display: flex;
            align-items: center;
            justify-content: center;
            min-height: 60px;
            font-size: 30px;
            padding-right: 10px;
            /* Adjust this value to create space between icon and text */
        }

        .sidebar li .icon {
            color: var(--primary-color);
            transition: var(--tran-02);
        }

        .sidebar li .text {
            color: var(--text-color);
            transition: var(--tran-02);
        }

        .sidebar header {
            position: relative;
        }

        .sidebar .image-text img {
            width: 40px;
            border-radius: 6px;
        }

        .sidebar header .image-text {
            display: flex;
            align-items: center;
        }

        header .image-text .header-text {
            display: flex;
            flex-direction: column;
        }

        .header-text .name {
            font-style: 600;
        }

        .header-text .profession {
            margin-top: -2px;
        }

        .sidebar header .toggle {
            position: absolute;
            top: 50%;
            right: -25px;
            transform: translateY(-50%) rotate(180deg);
            height: 25px;
            width: 25px;
            background: var(--primary-color);
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 50%;
            color: var(--sidebar-color);
            font-size: 22px;
            transition: var(--tran-03);
        }

        .sidebar li a {
            height: 100%;
            width: 100%;
            display: flex;
            align-items: center;
            text-decoration: none;
            border-radius: 6px;
            transition: var(--tran-04);
            padding: 10px;
            /* Add padding for better click area */
        }

        .sidebar li a:hover {
            background: var(--primary-color);
        }

        .sidebar li a:hover .icon,
        .sidebar li a:hover .text {
            color: var(--sidebar-color);
            align-items: center;
        }

        /* Additional styles for centering the content */
        .sidebar li .icon svg,
        .sidebar li .icon img {
            /* Adjust this to vertically align SVG icons or images within the container */
            vertical-align: middle;
        }

        .sidebar .menu-bar {
            height: calc(100% - 50px);
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .sidebar.close header .toggle {
            transform: translateY(-50%);
        }

        .home {
            position: relative;
            left: 250px;
            height: 100vh;
            width: calc(100% - 250px);
            background: var(--body-color);
            transition: var(--tran-05);
        }

        .home .text {
            font-size: 30px;
            font-weight: 500;
            color: var(--text-color);
            padding: 8px 40px;
        }

        /* .sidebar.sidebar.close~.home {
            left: 73px;
            width: calc(100% - 73px);
        } */

        .outer-container {
            width: 95%;
            /* Lebar 80% dari lebar halaman */
            margin: 0 auto;
            /* Untuk menengahkan container */
        }

        .inner-container {
            background-color: #ffffff;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            padding: 20px;
            overflow-x: auto;
            /* Untuk mengatasi responsifitas tabel */
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th,
        td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        /* Membuat warna latar belakang berganti untuk setiap baris */
        tbody tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        /* Style untuk header tabel */
        th {
            background-color: #7E3302;
            color: #ddd;
        }

        /* CSS untuk tombol warna coklat */

        .coklat-btn1 {
            background-color: #7E3302;
            /* Warna coklat */
            color: white;
            border: none;
            border-radius: 5px;
            box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4);
            /* Shadow */
            padding: 8px 12px;
            transition: background-color 0.3s ease;
            font-size: 15px;
        }

        .coklat-btn1:hover {
            background-color: #a54504;
            /* Warna coklat saat di-hover */
        }

        .coklat-btn {
            background-color: #7E3302;
            /* Warna coklat */
            color: white;
            border: none;
            border-radius: 5px;
            box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4);
            /* Shadow */
            padding: 8px 12px;
            transition: background-color 0.3s ease;
            font-size: 15px;
        }

        .coklat-btn:hover {
            background-color: #a54504;
            /* Warna coklat saat di-hover */
        }

        /* CSS untuk ikon trash */
        .coklat-btn .bx.bxs-trash {
            color: #FFF;
            /* Warna ikon trash */
        }

        /* CSS untuk tombol warna merah */
        .merah-btn {
            background-color: #B22222;
            /* Warna merah */
            color: white;
            border: none;
            border-radius: 5px;
            box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.4);
            /* Shadow */
            padding: 8px 12px;
            transition: background-color 0.3s ease;
            font-size: 15px;
        }

        .merah-btn:hover {
            background-color: #DC143C;
            /* Warna merah saat di-hover */
        }

        /* CSS untuk ikon edit */
        .merah-btn .bx.bxs-edit {
            color: #FFF;
            /* Warna ikon edit */
        }

        .container {
            width: 95%;
            margin: 0 auto;
            /* Untuk membuat container berada di tengah halaman */
            background-color: #fff;
            border-radius: 50px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
            /* Box shadow */
            padding: 20px;
        }

        h1 {
            text-align: center;
            margin-top: 0;
            color: var(--text-color);
            font-weight: 900;
        }

        .container-detail {
            display: flex;
            background-color: white;
            border-radius: 10px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            padding: 50px;
            max-width: 80%;
            margin: 0 auto;
        }

        .image-container {
            flex: 1;
            margin-right: 20px;
        }

        .gambar-produk {
            width: 100%;
            /* Gambar akan mengambil lebar container */
            border-radius: 5px;
        }

        .detail {
            flex: 1;
        }

        h2 {
            margin-top: 0;
            text-align: center;
            color: var(--text-color);
        }

        .checkout-btn {
            padding: 10px 20px;
            background-color: #7E3302;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-size: 16px;
            transition: background-color 0.3s ease;
            width: 100%;
            /* Gambar akan mengambil lebar container */

        }

        .checkout-btn:hover {
            background-color: #a54504;
        }

        .container-contact {
            background-color: #ffffff;
            border-radius: 10px;
            box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
            padding: 70px;
            width: 80%;
            margin: 0 auto;
            text-align: center;
            /* Untuk mengatur teks ke tengah */
            font-size: 20px
        }

        .contact-details {
            display: flex;
            flex-direction: column;
            gap: 10px;
        }

        .contact-item {
            display: flex;
            align-items: center;
            justify-content: center;
            color: var(--text-color);
        }

        .contact-item i {
            margin-right: 10px;
        }

        .contact-button {
            width: 80%;
            margin: 20px auto 0;
            /* Untuk membuat tombol berada di tengah */
            padding: 10px;
            background-color: #7E3302;
            color: #ffffff;
            border: none;
            border-radius: 30px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        .contact-button:hover {
            background-color: #a06020;
        }
    </style>
</head>
