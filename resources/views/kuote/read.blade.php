@extends('layoutss.main')

@section('content')

    <body>

        <section class="home"><br>
            <!-- <div class="text">Dashboard</div> -->
            <div class="container">
                <h1>DATA CONTACT CRATIVA</h1>
                <!-- Di sini Anda dapat menambahkan tabel data menu jika diperlukan -->
            </div><br><br>
            <div class="container-contact">
                <div class="contact-details">
                    @foreach ($kuote as $k)
                        <div class="contact-item">
                            <span><strong><i class='bx bxs-quote-left bx-tada' style='color:#7e3302' ></i>{{ $k->kuote }} <i class='bx bxs-quote-right bx-tada' style='color:#7e3302'  ></i></strong></span>
                        </div>
                    @endforeach
                </div>
                <a href="/kuote/update/{{ $k->ID_kuote }}"><button class="contact-button"><i class='bx bxs-edit bx-tada' ></i> Update data</button></a>
            </div>

        </section>
    </body>
@endsection
