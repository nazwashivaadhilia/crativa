<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INPUT DATA KUOTE</title>
</head>

<body>
    <h1>Tambah kuote</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="/kuote/storekuote" method="POST" enctype="multipart/form-data">
        @csrf

        <label for="ID_kuote">ID kuote:</label>
        <input type="text" name="ID_kuote" id="kode_head" value="{{ old('ID_kuote') }}"><br><br>

        <label for="judul">kuote:</label>
        <input type="text" name="kuote" id="judul" value="{{ old('kuote') }}"><br><br>

        <button type="submit">Simpan</button>
    </form>
</body>

</html>
