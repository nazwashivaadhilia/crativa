<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UPDATE DATA HEAD</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);


        /* CSS code for form size adjustment */
        .login-page {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        .form input:focus {
            border: 2px solid #7E3302;
            /* Ganti warna coklat sesuai keinginan Anda */
            /* Anda juga dapat menyesuaikan lebar atau gaya border sesuai kebutuhan */
        }

        .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 500px;
            /* Adjust the width of the form */
            margin: 0 auto 100px;
            padding: 60px;
            /* Adjust the padding for a larger form */
            text-align: center;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
            border-radius: 20px;
        }

        .form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 20px;
            /* Increase margin bottom for input fields */
            padding: 20px;
            /* Increase padding for input fields */
            box-sizing: border-box;
            font-size: 16px;
            /* Increase font size for input fields */
            border-radius: 10px;
        }

        .form button {
            text-transform: uppercase;
            outline: 0;
            background: #7E3302;
            width: 80%;
            border: 0;
            padding: 20px;
            /* Increase padding for the button */
            color: #FFFFFF;
            font-size: 15px;
            /* Increase font size for the button */
            font-weight: 900;
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
            cursor: pointer;
            border-radius: 50px;
            margin-bottom: 10px;
        }


        .form button:hover,
        .form button:active,
        .form button:focus {
            background: #a84401;
        }

        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

        .form .message a {
            color: #7E3302;
            text-decoration: none;
        }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

        .container:before,
        .container:after {
            content: "";
            display: block;
            clear: both;
        }

        .container .info {
            margin: 50px auto;
            text-align: center;
        }

        .container .info h1 {
            margin: 0 0 15px;
            padding: 0;
            font-size: 36px;
            font-weight: 300;
            color: #1a1a1a;
        }

        .container .info span {
            color: #4d4d4d;
            font-size: 12px;
        }

        .container .info span a {
            color: #000000;
            text-decoration: none;
        }


        body {
            background: #7E3302;
            /* fallback for old browsers */
            background: #7E3302;
            background: linear-gradient(90deg, #844b24 0%, #7E3302 50%);
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            font-family: 'Poppins', sans-serif;
        }

        .field-icon {
            float: right;
            margin-top: -28px;
            margin-right: 10px;
            position: relative;
            cursor: pointer;
        }
    </style>
</head>

<body><br><br>
    <div class="login-page">
        <div class="form">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="/head/update2/{{ $info->kode_head }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <input type="text" name="kode_head" value="{{ $info->kode_head }}" readonly />
                <input type="text" name="judul" value="{{ $info->judul }}" required />
                <input type="text" name="deskripsi" value="{{ $info->deskripsi }}" required />
                <input type="file" type="file" name="gambar" />
                <button type="submit">Save</button>
            </form>
            <a href="/head/read"><button type="button">Back</button></a>
        </div>
    </div>

</body>

</html>
