@extends('layoutss.main')

@section('content')

    <body>

        <section class="home"><br>
            <!-- <div class="text">Dashboard</div> -->
            <div class="container">
                <h1>HEAD CRATIVA</h1>
                <!-- Di sini Anda dapat menambahkan tabel data menu jika diperlukan -->
            </div><br>
            <div class="container-detail">
                <div class="image-container">
                    @foreach ($heads as $h)
                        <img src="{{ asset('images/' . $h->gambar) }}" alt="Gambar" class="gambar-produk">
                    @endforeach
                </div>
                <div class="detail">
                    @foreach ($heads as $h)
                        <h2>{{ $h->judul }}</h2><br><br><br>
                    @endforeach
                    @foreach ($heads as $h)
                        <p>
                            <font color="#70707"><strong>DESKRIPSI:</strong> <br>{{ $h->deskripsi }}
                        </p>
                    @endforeach
                    <br>
                    </font>
                    <a href="/head/editt/{{ $h->kode_head }}"><button class="checkout-btn"><i class='bx bxs-edit bx-tada' ></i> Update data</button></a>
                </div>
            </div>

        </section>
    </body>
@endsection
