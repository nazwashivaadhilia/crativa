<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INPUT DATA HEAD</title>
</head>

<body>
    <h1>Tambah head</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="/head/store2" method="POST" enctype="multipart/form-data">
        @csrf

        <label for="kode_data">Kode head:</label>
        <input type="text" name="kode_head" id="kode_head" value="{{ old('kode_head') }}"><br><br>

        <label for="judul">Judul:</label>
        <input type="text" name="judul" id="judul" value="{{ old('judul') }}"><br><br>

        <label for="deskripsi">Deskripsi:</label><br>
        <textarea name="deskripsi" id="deskripsi" rows="4">{{ old('deskripsi') }}</textarea><br><br>

        <label for="gambar">Gambar:</label>
        <input type="file" name="gambar" id="gambar"><br><br>

        <button type="submit">Simpan</button>
    </form>
</body>

</html>
