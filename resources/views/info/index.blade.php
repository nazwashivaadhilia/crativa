@extends('layoutss.main')

@section('content')

    <body>

        <section class="home"><br>
            <!-- <div class="text">Dashboard</div> -->
            <div class="container">
                <h1>INFO CRATIVA</h1>
                <!-- Di sini Anda dapat menambahkan tabel data menu jika diperlukan -->
            </div><br>
            <div class="container-detail">
                <div class="image-container">
                    @foreach ($infos as $info)
                        <img src="{{ asset('images/' . $info->gambar) }}" alt="Gambar" class="gambar-produk">
                    @endforeach
                </div>
                <div class="detail">
                    @foreach ($infos as $info)
                        <h2>{{ $info->judul }}</h2><br>
                    @endforeach
                    @foreach ($infos as $info)
                        <p>
                            <font color="#70707"><strong>DESKRIPSI:</strong> <br>{{ $info->deskripsi }}
                        </p>
                    @endforeach
                    <br>
                    @foreach ($infos as $info)
                        <p><strong>SUB-DESKRIPSI:</strong> <br> {{ $info->keterangan }}</p>
                    @endforeach
                    <br>
                    </font>
                    <a href="/info/edit/{{ $info->kode_data }}"><button class="checkout-btn"><i class='bx bxs-edit bx-tada' ></i> Update data</button></a>
                </div>
            </div>

        </section>
    </body>
@endsection
