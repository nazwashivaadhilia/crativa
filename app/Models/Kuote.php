<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kuote extends Model
{
    use HasFactory;

    protected $table = 'kuote';
    protected $fillable = ['ID_kuote', 'kuote'];
}
