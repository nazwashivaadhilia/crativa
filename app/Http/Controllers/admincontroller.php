<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Head;
use App\Models\Kuote;
use App\Models\Info;
use App\Models\Contact;

class admincontroller extends Controller
{
    // Controller dashboard
    public function dashboard()
    {
        return view('dashboard');
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // controller CRUD menu
    public function menushow()
    {
        $menu = DB::table('menu')->get();
        return view('menus.showmenu', ['menu' => $menu]);
    }

    public function create()
    {
        return view('menus.create');
    }

    public function store(Request $request)
    {
        DB::table('menu')->insert([
            'ID_menu' => $request->ID_menu,
            'No_menu' => $request->No_menu,
            'Nama_menu' => $request->Nama_menu,
            'Deskripsi' => $request->Deskripsi,
            'Harga' => $request->Harga
        ]);
        return redirect('/menus/showmenu');
    }

    public function delete($id)
    {
        DB::table('menu')->where('ID_menu', $id)->delete();
        return redirect('/menus/showmenu');
    }

    public function edit($id)
    {
        $menu = DB::table('menu')->where('ID_menu', $id)->get();
        return view('menus/update', ['menu' => $menu]);
    }

    public function update(Request $request)
    {
        $id_menu = $request->input('ID_menu');

        $affected = DB::table('menu')->where('ID_menu', $id_menu)->update([
            'No_menu' => $request->input('No_menu'),
            'Nama_menu' => $request->input('Nama_menu'),
            'Deskripsi' => $request->input('Deskripsi'),
            'Harga' => $request->input('Harga')
        ]);

        return redirect('/menus/showmenu');
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // controller CRU info
    public function index1()
    {
        $infos = Info::all();
        return view('info.index', compact('infos'));
    }

    public function create1()
    {
        return view('info.create');
    }

    public function store1(Request $request)
    {
        $data = $request->validate([
            'kode_data' => 'required|unique:info',
            'judul' => 'required',
            'deskripsi' => 'required',
            'keterangan' => 'required',
            'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048' // validasi untuk gambar
        ]);

        if ($request->hasFile('gambar')) {
            $image = $request->file('gambar');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $data['gambar'] = $imageName;
        }

        Info::create($data);

        return redirect('/info/index')->with('success', 'Info berhasil ditambahkan.');
    }

    public function edit1($kode_data)
    {
        $info = Info::where('kode_data', $kode_data)->firstOrFail();
        return view('info.edit', compact('info'));
    }

    public function update1(Request $request, $kode_data)
    {
        $info = Info::where('kode_data', $kode_data)->firstOrFail();

        $data = $request->validate([
            'kode_data' => 'required|unique:info,kode_data,' . $info->id,
            'judul' => 'required',
            'deskripsi' => 'required',
            'keterangan' => 'required',
            'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048' // validasi untuk gambar
        ]);

        if ($request->hasFile('gambar')) {
            $image = $request->file('gambar');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $data['gambar'] = $imageName;
        }

        $info->update($data);

        return redirect('/info/index')->with('success', 'Info berhasil diperbarui.');
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // controller CRU head
    public function index2()
    {
        $heads = Head::all();
        return view('head.read', compact('heads'));
    }

    public function add()
    {
        return view('head.add');
    }

    public function store2(Request $request)
    {
        $data =
            $request->validate([
                'kode_head' => 'required|unique:heads',
                'judul' => 'required',
                'deskripsi' => 'required',
                'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048' // validasi untuk gambar
            ]);

        if ($request->hasFile('gambar')) {
            $image = $request->file('gambar');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $data['gambar'] = $imageName;
        }

        Head::create($data);

        return redirect('/head/read')->with('success', 'Head berhasil ditambahkan.');
    }

    public function edit2($kode_head)
    {
        $info = Head::where('kode_head', $kode_head)->firstOrFail();
        return view('head.editt', compact('info'));
    }

    public function update2(Request $request, $kode_head)
    {
        $info = Head::where('kode_head', $kode_head)->firstOrFail();

        $data = $request->validate([
            'kode_head' => 'required|unique:heads,kode_head,' . $info->id,
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048' // validasi untuk gambar
        ]);

        if ($request->hasFile('gambar')) {
            $image = $request->file('gambar');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $imageName);
            $data['gambar'] = $imageName;
        }

        $info->update($data);

        return redirect('/head/read')->with('success', 'Info berhasil diperbarui.');
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //controller CRU kuote
    public function showkuote()
    {
        $kuote = DB::table('kuote')->get();
        return view('kuote.read', ['kuote' => $kuote]);
    }

    public function createkuote()
    {
        return view('kuote.create');
    }

    public function storekuote(Request $request)
    {
        $data =
            $request->validate([
                'ID_kuote' => 'required|unique:kuote',
                'kuote' => 'required',
            ]);

        Kuote::create($data);

        return redirect('/kuote/read')->with('success', 'kuote berhasil ditambahkan.');
    }

    public function updatekuote($ID_kuote)
    {
        $info = Kuote::where('ID_kuote', $ID_kuote)->firstOrFail();
        return view('kuote.update', compact('info'));
    }

    public function updatekuotep(Request $request, $ID_kuote)
    {
        $info = Kuote::where('ID_kuote', $ID_kuote)->firstOrFail();

        $data = $request->validate([
            'ID_kuote' => 'required|unique:kuote,ID_kuote,' . $info->id,
            'kuote' => 'required',
        ]);

        $info->update($data);

        return redirect('/kuote/read')->with('success', 'kuote berhasil diperbarui.');
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //controller CRU contact
    public function showcontact()
    {
        $contact = DB::table('contact')->get();
        return view('contact.read', ['contact' => $contact]);
    }

    public function createcontact()
    {
        return view('contact.create');
    }

    public function storecontact(Request $request)
    {
        $data =
            $request->validate([
                'ID_contact' => 'required|unique:contact',
                'detail' => 'required',
                'des' => 'required',
                'no' => 'required',
                'email' => 'required',
                'loc' => 'required',
            ]);

        Contact::create($data);

        return redirect('/contact/read')->with('success', 'contact berhasil ditambahkan.');
    }

    public function updatecontact($ID_contact)
    {
        $info = Contact::where('ID_contact', $ID_contact)->firstOrFail();
        return view('contact.update', compact('info'));
    }

    public function updatecontactp(Request $request, $ID_contact)
    {
        $info = Contact::where('ID_contact', $ID_contact)->firstOrFail();

        $data = $request->validate([
            'ID_contact' => 'required|unique:contact,ID_contact,' . $info->id,
            'detail' => 'required',
            'des' => 'required',
            'no' => 'required',
            'email' => 'required',
            'loc' => 'required',
        ]);

        $info->update($data);

        return redirect('/contact/read')->with('success', 'contact berhasil diperbarui.');
    }
}
