<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Info;
use App\Models\Head;
use App\Models\Kuote;
use App\Models\Contact;

class maincontroller extends Controller
{
    public function LandingPage()
    {
        $contact = Contact::all();
        $kuote = Kuote::all();
        $heads = Head::all();
        $infos = Info::all();
        $menu = Menu::all();
        return view('Landingpage', compact('menu', 'infos','heads','kuote','contact'));
    }
}
