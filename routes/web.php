<?php

use App\Http\Controllers\admincontroller;
use App\Http\Controllers\maincontroller;
use App\Http\Controllers\SessionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('sesi/index');
});

//halaman Landingpage
Route::get('/Landingpage', [maincontroller::class, 'LandingPage'])->middleware('isTamu');
//halaman awal dashboard
Route::get('/dashboard', [admincontroller::class, 'dashboard'])->middleware('isLogin');

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// route login, register, logout
Route::get('/sesi', [SessionController::class, 'index'])->middleware('isTamu');
Route::get('/sesi/logout', [SessionController::class, 'logout']);
Route::post('/sesi/login', [SessionController::class, 'login'])->middleware('isTamu');
Route::get('/sesi/register', [SessionController::class, 'register'])->middleware('isTamu');
Route::post('/sesi/create', [SessionController::class, 'create'])->middleware('isTamu');

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//route CRUD menu
Route::get('/menus/showmenu', [admincontroller::class, 'menushow'])->middleware('isLogin');
Route::get('/menus/create', [admincontroller::class, 'create'])->middleware('isLogin');
Route::post('/menu/store', [admincontroller::class, 'store'])->middleware('isLogin');
Route::get('/menu/delete/{id}', [admincontroller::class, 'delete'])->middleware('isLogin');
Route::get('/menus/update/{id}', [admincontroller::class, 'edit'])->middleware('isLogin');
Route::post('/menu/update', [admincontroller::class, 'update'])->middleware('isLogin');

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// route CRU info
Route::get('/info/index', [admincontroller::class, 'index1'])->middleware('isLogin');
Route::get('/info/create', [admincontroller::class, 'create1'])->middleware('isLogin');
Route::post('/info/store1', [admincontroller::class, 'store1'])->middleware('isLogin');
Route::get('/info/edit/{kode_data}', [admincontroller::class, 'edit1'])->middleware('isLogin');
Route::put('/info/update1/{kode_data}', [AdminController::class, 'update1'])->middleware('isLogin');

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// route CRU head
Route::get('/head/read', [admincontroller::class, 'index2'])->middleware('isLogin');
Route::get('/head/add', [admincontroller::class, 'add'])->middleware('isLogin');
Route::post('/head/store2', [admincontroller::class, 'store2'])->middleware('isLogin');
Route::get('/head/editt/{kode_head}', [admincontroller::class, 'edit2'])->middleware('isLogin');
Route::put('/head/update2/{kode_head}', [AdminController::class, 'update2'])->middleware('isLogin');

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// route CRU kuote
Route::get('/kuote/read', [admincontroller::class, 'showkuote'])->middleware('isLogin');
Route::get('/kuote/create', [admincontroller::class, 'createkuote'])->middleware('isLogin');
Route::post('/kuote/storekuote', [admincontroller::class, 'storekuote'])->middleware('isLogin');
Route::get('/kuote/update/{ID_kuote}', [admincontroller::class, 'updatekuote'])->middleware('isLogin');
Route::put('/kuote/updatekuotep/{ID_kuote}', [AdminController::class, 'updatekuotep'])->middleware('isLogin');

// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// route CRU contact
Route::get('/contact/read', [admincontroller::class, 'showcontact'])->middleware('isLogin');
Route::get('/contact/create', [admincontroller::class, 'createcontact'])->middleware('isLogin');
Route::post('/contact/storecontact', [admincontroller::class, 'storecontact'])->middleware('isLogin');
Route::get('/contact/update/{ID_contact}', [admincontroller::class, 'updatecontact'])->middleware('isLogin');
Route::put('/contact/updatecontactp/{ID_contact}', [AdminController::class, 'updatecontactp'])->middleware('isLogin');












Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
